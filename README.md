# siasnap

Siasnap is a template for a simple folder snapshot creation tool.

It has 3 available commands:

- `create [dir]` - creates a snapshot of the dir on Sia
- `list [dir]` - lists all available snapshots for a dir
- `restore [dir] [id]` restores the snapshot with the specified id for a dir

The implementations for these commands should be added in `create.go`,
`list.go` and `restore.go` respectively. `helpers.go` contains useful helper
methods which can be used for things like creating siapaths and
taring/untaring a dir.

## Testing

To check if your implementation is correct, you can run `make test-create`,
`make test-list` and `make test-restore`. Assuming that you followed the
[requirements](#implementation-requirements) the tests should tell you if
your implementation is correct.
Once all commands are implemented run `make test-all` to run an integration
test that combines all of your implemented commands within a single test.

## Implementation Requirements

This repo is meant to be forked and then implemented. You can use the
following checklist to track your progress.

- [ ] [`siasnap create`](#create)
- [ ] [`siasnap list`](#list)
- [ ] [`siasnap restore`](#restore)

## Create

`siasnap create [dir]` is supposed to create a tarball at a temporary
location on your filesystem and upload it to Sia. For the automated tests to
pass, the tarball needs to be uploaded to the correct SiaPath. Assuming that
your folder is located at `/home/dir` the resulting tarball should be
uploaded to `/siasnap/home/dir/[index]` where `[index]` should be replaced
with a number starting from 0 for the first snapshot of `/home/dir`. That
means the second snapshot would be uploaded to the path
`/siasnap/home/dir/1`. This `[index]` will later be referred to as `id`.

## List

`siasnap list [dir]` returns a list of all the existing snapshot `ids` (the
`index` from before) and the `CreatedAt` timestamp for each one. The format
of the output doesn't matter for the automated tests but the return values of
`list` do.

## Restore

`siasnap restore [dir] [id]` restores the snapshot with `id` of directory
`dir`. The restoration process should remove all contents of `dir` and
replace them to match the contents of the snapshot.

## Docs

Most applications interact with Sia through an http API. For apps written in
golang, you can use the Sia client library. Every call in the API has a
corresponding method in the client library.

We recomment using the Sia API documentation to get oriented, as it has the best
descriptions and numerous examples.

- [REST API](https://sia.tech/docs)
- [Go Client Library](https://godoc.org/gitlab.com/NebulousLabs/Sia/node/api/client)

If you need help setting up a golang environment, you can use the link below:

[Go: Getting Started](https://golang.org/doc/install)