package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/NebulousLabs/fastrand"

	"gitlab.com/NebulousLabs/errors"
)

// TestHelpers runs all the helper tests.
func TestHelpers(t *testing.T) {
	// Test dirToSiaPath
	t.Run("TestDirToSiaPath", testDirToSiaPath)
	// Test tarDir and untarDir
	t.Run("TestDirToSiaPath", testTarUntarDir)
}

// testDirToSiaPath unit tests the dirToSiaPath function.
func testDirToSiaPath(t *testing.T) {
	t.Parallel()
	// Get the current working dir.
	wd, err := filepath.Abs("./")
	if err != nil {
		t.Fatal(err)
	}
	wd = filepath.ToSlash(wd)
	wd = strings.TrimPrefix(wd, filepath.VolumeName(wd))
	wd = strings.Trim(wd, "/")
	// Define a few tests.
	tests := []struct {
		dir     string
		siaPath string
	}{
		{
			dir:     "/home/user",
			siaPath: fmt.Sprintf("%v/home/user", appName),
		},
		{
			dir:     "C:\\home\\user",
			siaPath: fmt.Sprintf("%v/home/user", appName),
		},
		{
			dir:     "./home/user",
			siaPath: fmt.Sprintf("%v/%v/home/user", appName, wd),
		},
	}
	// Run the tests.
	for _, test := range tests {
		sp, err := dirToSiaPath(test.dir)
		if err != nil {
			t.Fatal(err)
		}
		if sp.String() != test.siaPath {
			t.Errorf("Expected SiaPath %v but got %v", test.siaPath, sp.String())
		}
	}
}

// testTarUntarDir tests the tarDir and untarDir functions.
func testTarUntarDir(t *testing.T) {
	t.Parallel()
	// Prepare dir for testing.
	testDir := filepath.Join(os.TempDir(), appName, t.Name())
	err1 := os.RemoveAll(testDir)
	err2 := os.MkdirAll(testDir, 0700)
	if err := errors.Compose(err1, err2); err != nil {
		t.Fatal(err)
	}
	// Create a dir to create a snapshot of in the testDir.
	dir := filepath.Join(testDir, "dir")
	if err := os.MkdirAll(dir, 0700); err != nil {
		t.Fatal(err)
	}
	subDir := filepath.Join(dir, "subdir")
	if err := os.MkdirAll(subDir, 0700); err != nil {
		t.Fatal(err)
	}
	// Create a file in the subdir.
	file := filepath.Join(subDir, "foo")
	fileContent := fastrand.Bytes(100)
	if err := ioutil.WriteFile(file, fileContent, 0700); err != nil {
		t.Fatal(err)
	}
	// Tar the dir.
	dst := filepath.Join(testDir, "archive.tar.gz")
	if err := tarDir(dir, dst); err != nil {
		t.Fatal(err)
	}
	// Check if the file was created.
	if _, err := os.Stat(dst); err != nil {
		t.Fatal(err)
	}
	// Modify the file in the dir and add another file.
	if err := ioutil.WriteFile(file, fastrand.Bytes(100), 0700); err != nil {
		t.Fatal(err)
	}
	if err := ioutil.WriteFile(filepath.Join(subDir, "foo2"), fastrand.Bytes(100), 0700); err != nil {
		t.Fatal(err)
	}
	// Restore the snapshot.
	if err := untarDir(dst, dir); err != nil {
		t.Fatal(err)
	}
	// The original file should exist with the correct content.
	readContent, err := ioutil.ReadFile(file)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(fileContent, readContent) {
		t.Fatal("readContent doesn't match fileContent")
	}
	// Check that the dir and subdir only have element in it.
	fis, err := ioutil.ReadDir(dir)
	if err != nil {
		t.Fatal(err)
	}
	if len(fis) != 1 {
		t.Fatalf("Expected 1 fileinfo but got %v", len(fis))
	}
	fis, err = ioutil.ReadDir(subDir)
	if err != nil {
		t.Fatal(err)
	}
	if len(fis) != 1 {
		t.Fatalf("Expected 1 fileinfo but got %v", len(fis))
	}
}
