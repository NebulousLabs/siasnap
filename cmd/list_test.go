package cmd

import (
	"bytes"
	"fmt"
	"path/filepath"
	"sort"
	"testing"

	"gitlab.com/NebulousLabs/fastrand"
)

// TestList tests the 'list' function by manually uploading a file first to the
// expected location of a snapshot.
func TestList(t *testing.T) {
	// Setup testgroup.
	tg, testDir, err := setupTestGroup(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	// Create a path to a folder. It doesn't need to be created.
	dir := filepath.Join(testDir, "dir")
	fileContent := fastrand.Bytes(100)
	// Get the siapath of the dir.
	dirSiaPath, err := dirToSiaPath(dir)
	if err != nil {
		t.Fatal(err)
	}
	// Run this test multiple times to create multiples snapshots.
	renter := tg.Renters()[0]
	for i := 0; i < 3; i++ {
		// Upload a fake snapshot to the dir.
		snapSiaPath, err := dirSiaPath.Join(fmt.Sprint(i))
		if err != nil {
			t.Fatal(err)
		}
		if err := renter.RenterUploadStreamPost(bytes.NewReader(fileContent), snapSiaPath, 1, 1, false); err != nil {
			t.Fatal(err)
		}
		// List the snapshots with 'list'.
		snapshots, err := list(dir)
		if err != nil {
			t.Fatal("Calling 'list' returned an error", err)
		}
		if len(snapshots) != i+1 {
			t.Fatalf("Expected %v snapshots but got %v", i+1, len(snapshots))
		}
		sort.Slice(snapshots, func(i, j int) bool {
			return snapshots[i].createdAt.Before(snapshots[j].createdAt)
		})
		// Get the fileinfo of the snapshot on Sia.
		fi, err := renter.RenterFileGet(snapSiaPath)
		if err != nil {
			t.Fatal(err)
		}
		// Compare the CreatedAt timestamp to the one returned by 'list'.
		if !fi.File.CreateTime.Equal(snapshots[i].createdAt) {
			t.Fatalf("Expected 'createdAt' to be %v but was %v", fi.File.CreateTime, snapshots[i])
		}
	}
}
