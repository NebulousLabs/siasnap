package cmd

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/NebulousLabs/Sia/modules"
)

// checkNumArgs exits the program if the number of arguments doesn't match the
// expected number.
func checkNumArgs(args []string, n int) {
	if len(args) != n {
		fmt.Printf("Invalid number of args: %v != %v", len(args), n)
		os.Exit(1)
	}
}

// dirToSiaPath turns the path to a directory into the SiaPath of the
// corresponding dir on the Sia network.
func dirToSiaPath(dir string) (modules.SiaPath, error) {
	// Get absolute path.
	dir, err := filepath.Abs(dir)
	if err != nil {
		return modules.SiaPath{}, err
	}
	// Remove the volume name.
	dir = strings.TrimPrefix(dir, filepath.VolumeName(dir))
	// Prepend path with "siasnap" and turn into SiaPath.
	return modules.NewSiaPath(filepath.Join(appName, dir))
}

// tarDir tars the directory at src and creates a tarball at dst.
func tarDir(src, dst string) error {
	// Check that src exists and that it is a dir.
	if fi, err := os.Stat(src); err != nil {
		return err
	} else if !fi.IsDir() {
		return errors.New("src isn't a dir")
	}
	// Open the dst.
	dstFile, err := os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
	if err != nil {
		return err
	}
	defer dstFile.Close()
	// Wrap file into gzip writer.
	gzw := gzip.NewWriter(dstFile)
	defer gzw.Close()

	// Wrap gzip writer into tarball writer.
	tw := tar.NewWriter(gzw)
	defer tw.Close()

	// Walk over dir.
	return filepath.Walk(src, func(file string, fi os.FileInfo, err error) error {
		// Stop at any error.
		if err != nil {
			return err
		}
		// Create header.
		header, err := tar.FileInfoHeader(fi, fi.Name())
		if err != nil {
			return err
		}
		// Update the name to
		header.Name = strings.TrimPrefix(strings.Replace(file, src, "", -1), string(filepath.Separator))
		// Write header.
		if err := tw.WriteHeader(header); err != nil {
			return err
		}
		// Return on non-regular files.
		if !fi.Mode().IsRegular() {
			return nil
		}
		// Open files for taring.
		f, err := os.Open(file)
		if err != nil {
			return err
		}
		// Copy file into tarball.
		if _, err := io.Copy(tw, f); err != nil {
			_ = f.Close()
			return err
		}
		// Close file.
		return f.Close()
	})
}

// untarDir untars the tarball at src and moves its contents into dst, deleting any existing files.
func untarDir(src, dst string) error {
	// Make sure the dst dir exists at first.
	if fi, err := os.Stat(dst); err != nil {
		return err
	} else if !fi.IsDir() {
		return errors.New("dst dir isn't a directory")
	}
	// Delete the dst dir.
	if err := os.RemoveAll(dst); err != nil {
		return err
	}
	// Recreate the dst dir.
	if err := os.Mkdir(dst, 0755); err != nil {
		return err
	}
	// Open tarball.
	tarFile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer tarFile.Close()
	// Wrap it in a gzip reader.
	gzr, err := gzip.NewReader(tarFile)
	if err != nil {
		return err
	}
	defer gzr.Close()
	// Wrap the gzr in a tarball reader.
	tr := tar.NewReader(gzr)
	// Untar dir.
	for {
		header, err := tr.Next()
		switch {
		case err == io.EOF:
			return nil // done
		case err != nil:
			return err
		case header == nil:
			continue
		}
		// Build target path.
		target := filepath.Join(dst, header.Name)
		// If target is a folder, create it.
		if header.FileInfo().IsDir() {
			if err := os.MkdirAll(target, 0755); err != nil {
				return err
			}
			continue
		}
		// If it is a file, create it.
		targetFile, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR|os.O_EXCL, header.FileInfo().Mode())
		if err != nil {
			return err
		}
		if _, err := io.Copy(targetFile, tr); err != nil {
			_ = targetFile.Close()
			return err
		}
		_ = targetFile.Close()
	}
}
