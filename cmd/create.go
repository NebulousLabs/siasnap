package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var createCmd = &cobra.Command{
	Use:   "create [dir]",
	Short: "create creates a snapshot of a directory",
	Long: `create creates a snapshot of a directory by 
	taring the directory to a temporary location and uploading it to Sia`,
	Run: func(cmd *cobra.Command, args []string) {
		checkNumArgs(args, 1)
		fmt.Printf("INFO: 'create' called with dir '%v'\n\n", args[0])
		if err := create(args[0]); err != nil {
			println(err.Error())
			os.Exit(1)
		}
	},
}

// create contains the implementation of the 'create' command.
func create(dir string) error {
	// TODO: turn the local path of 'dir' into a SiaPath.
	// TIP: You can use 'dirToSiaPath' in helpers.go for that.

	// TODO: Tar the dir to a temporary location.
	// TIP: Use 'tarDir' from helpers.go

	// TODO: Get the SiaPath of the new snapshot uploaded into that directory. Use
	// an incrementing counter for each snapshot of that dir. dir/0, dir/1, dir/2
	// etc.
	// TIP: the SiaPath type has a Join method that can be used like this:
	// dirSiaPath.Join("0")

	// TODO: Upload the Tar to Sia.
	// TIP: Use the global siaClient. The uploading methods start with the prefix
	// 'RenterUpload'. For the tests to pass it is recommended to use
	// RenterUploadDefaultPost.
	println("'create' is not implemented yet")
	return nil
}
