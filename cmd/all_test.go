package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/Sia/build"
	"gitlab.com/NebulousLabs/Sia/siatest"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
)

// setTestGroup creates a simple testgroup.
func setupTestGroup(testName string) (*siatest.TestGroup, string, error) {
	// Define group for testing.
	groupParams := siatest.GroupParams{
		Hosts:   3,
		Renters: 1,
		Miners:  1,
	}
	// Prepare the testDir.
	testDir := filepath.Join(os.TempDir(), appName, testName)
	err1 := os.RemoveAll(testDir)
	err2 := os.MkdirAll(testDir, 0700)
	if err := errors.Compose(err1, err2); err != nil {
		return nil, "", err
	}
	// Create group.
	tg, err := siatest.NewGroupFromTemplate(filepath.Join(testDir, "sia"), groupParams)
	if err != nil {
		return nil, "", err
	}
	// Update the client's address to match the renter's.
	siaClient.Address = tg.Renters()[0].Address
	siaClient.Password = tg.Renters()[0].Password
	return tg, testDir, nil
}

// TestAll is an integration test which uses all of the functions implemented by
// the user.
func TestAll(t *testing.T) {
	// Setup testgroup.
	tg, testDir, err := setupTestGroup(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	// Create a folder within the testDir with a single file.
	dir := filepath.Join(testDir, "dir")
	if err := os.MkdirAll(dir, 0700); err != nil {
		t.Fatal(err)
	}
	file := filepath.Join(dir, "foo")
	fileContent := fastrand.Bytes(100)
	if err := ioutil.WriteFile(file, fileContent, 0700); err != nil {
		t.Fatal(err)
	}
	// Get the siapath of the dir.
	dirSiaPath, err := dirToSiaPath(dir)
	if err != nil {
		t.Fatal(err)
	}
	// Run this test multiple times to create multiples snapshots.
	renter := tg.Renters()[0]
	for i := 0; i < 3; i++ {
		// Create a snapshot of dir using 'create'.
		if err := create(dir); err != nil {
			t.Fatal("Calling 'create' returned an error", err)
		}
		// List the snapshots with 'list'.
		snapshots, err := list(dir)
		if err != nil {
			t.Fatal("Calling 'list' returned an error", err)
		}
		if len(snapshots) != i+1 {
			t.Fatalf("Expected %v snapshots but got %v", i+1, len(snapshots))
		}
		sort.Slice(snapshots, func(i, j int) bool {
			return snapshots[i].createdAt.Before(snapshots[j].createdAt)
		})
		// Get the fileinfo of the snapshot to compare the timestamp.
		snapSiaPath, err := dirSiaPath.Join(fmt.Sprint(i))
		if err != nil {
			t.Fatal(err)
		}
		// Wait for the file to be healthy.
		err = build.Retry(1000, 100*time.Millisecond, func() error {
			// Get the fileinfo.
			file, err := renter.RenterFileGet(snapSiaPath)
			if err != nil {
				t.Fatal(err)
			}
			// Check that the file is healthy.
			if file.File.MaxHealth > 1 {
				return fmt.Errorf("file is not healthy yet, threshold is %v but health is %v", 1, file.File.MaxHealth)
			}
			return nil
		})
		if err != nil {
			t.Fatal(err)
		}
		fi, err := renter.RenterFileGet(snapSiaPath)
		if err != nil {
			t.Fatal(err)
		}
		// Compare the CreatedAt timestamp to the one returned by 'list'.
		if !fi.File.CreateTime.Equal(snapshots[i].createdAt) {
			t.Fatalf("Expected 'createdAt' to be %v but was %v", fi.File.CreateTime, snapshots[i])
		}
		// Replace the content of the file with new content.
		newContent := fastrand.Bytes(100)
		err = ioutil.WriteFile(file, newContent, 0700)
		if err != nil {
			t.Fatal(err)
		}
		// Recover the latest snapshot.
		if err := restore(dir, fmt.Sprint(i)); err != nil {
			t.Fatal("Calling 'recover' returned an error", err)
		}
		// Check dir.
		fis, err := ioutil.ReadDir(dir)
		if err != nil {
			t.Fatal(err)
		}
		if len(fis) != 1 {
			t.Fatalf("Snapshot contained %v file(s) but want %v", len(fis), 1)
		}
		if fis[0].Name() != filepath.Base(file) {
			t.Fatalf("Snapshot in file should have name %v but had name %v", filepath.Base(file), fis[0].Name())
		}
		readContent, err := ioutil.ReadFile(file)
		if err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(readContent, fileContent) {
			t.Log("Read content: ", readContent)
			t.Log("Original content: ", fileContent)
			t.Fatal("content of file in snapshot doesn't match original content")
		}
	}
}
