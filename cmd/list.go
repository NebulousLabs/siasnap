package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:   "list [dir]",
	Short: "list lists the available snapshots of a directory",
	Long: `list lists the available snapshots of a directory which have
	been created using the 'create' command`,
	Run: func(cmd *cobra.Command, args []string) {
		checkNumArgs(args, 1)
		fmt.Printf("'list' called with dir '%v'\n\n", args[0])
		if _, err := list(args[0]); err != nil {
			println(err.Error())
			os.Exit(1)
		}
	},
}

// snapshot is definition of the return type of the 'list' method.
type snapshot struct {
	id        string
	createdAt time.Time
}

// list contains the implementation of the 'list' command.
func list(dir string) ([]snapshot, error) {
	// TODO: turn the local path of 'dir' into a SiaPath.
	// TIP: You can use 'dirToSiaPath' in helpers.go for that.

	// TODO: Use the api to fetch all the snapshots within the uploaded dir and
	// print their name/id (the one you gave them in 'create') and 'created'
	// timestamp.
	// TIP: Use the siaClient.RenterGetDir command.

	// TODO: Print a list of available snapshots.

	// TODO: Return the available snapshots for the automated tests.
	println("'list' is not implemented yet")
	return nil, nil
}
