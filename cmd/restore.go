package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var restoreCmd = &cobra.Command{
	Use:   "restore [dir] [snapshotid]",
	Short: "restore restores the dir to the snapshot with the available id",
	Long: `restore restores the dir to the snapshot with the availale id. 
	Snapshots for a dir with their corresponding ID can be retrieved by using the 'list' command`,
	Run: func(cmd *cobra.Command, args []string) {
		checkNumArgs(args, 2)
		fmt.Printf("'restore' called with dir '%v' and id '%v'\n\n", args[0], args[1])
		if err := restore(args[0], args[1]); err != nil {
			println(err.Error())
			os.Exit(1)
		}
	},
}

// restore contains the implementation of the 'restore' command.
func restore(dir, snapshotid string) error {
	// TODO: turn the local path of 'dir' into a SiaPath.
	// TIP: You can use 'dirToSiaPath' in helpers.go for that.

	// TODO: Get the SiaPath of the snapshot.

	// TODO: Download the dir to a temporary location.

	// TODO: Untar the snapshot into 'dir'.
	// TIP: Use 'untarDir' from helpers.go
	println("'restore' is not implemented yet")
	return nil
}
