package cmd

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/Sia/build"
	"gitlab.com/NebulousLabs/fastrand"
)

// TestCreate tests if creating multiple snapshots of a dir with the 'create'
// command works as expected.
func TestCreate(t *testing.T) {
	// Setup testgroup.
	tg, testDir, err := setupTestGroup(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	// Create a folder within the testDir with a single file.
	dir := filepath.Join(testDir, "dir")
	if err := os.MkdirAll(dir, 0700); err != nil {
		t.Fatal(err)
	}
	file := filepath.Join(dir, "foo")
	fileContent := fastrand.Bytes(100)
	if err := ioutil.WriteFile(file, fileContent, 0700); err != nil {
		t.Fatal(err)
	}
	// Get the siapath of the dir.
	dirSiaPath, err := dirToSiaPath(dir)
	if err != nil {
		t.Fatal(err)
	}
	// Run this test multiple times to create multiples snapshots.
	r := tg.Renters()[0]
	for i := 0; i < 3; i++ {
		// Prepare a temp location for the downloaded tarball.
		tmpFile := filepath.Join(testDir, hex.EncodeToString(fastrand.Bytes(16))+"-tmp.tar.gz")
		// Create a snapshot of dir using 'create'.
		if err := create(dir); err != nil {
			t.Fatal("Calling 'create' returned an error", err)
		}
		// Check if the snapshot was uploaded to the correct path.
		rd, err := r.RenterGetDir(dirSiaPath)
		if err != nil {
			t.Fatal("Failed to retrieve snapshot dir", err)
		}
		sort.Slice(rd.Files, func(i, j int) bool {
			return rd.Files[i].CreateTime.Before(rd.Files[j].CreateTime)
		})
		if len(rd.Files) != i+1 {
			t.Fatalf("Expected %v snapshot in the dir but got %v", i+1, len(rd.Files))
		}
		if rd.Files[i].SiaPath.Name() != fmt.Sprint(i) {
			t.Fatalf("Expected name of snapshot to be %v but was %v", i, rd.Files[i].SiaPath.Name())
		}
		// Replace the content of the file with new content.
		newContent := fastrand.Bytes(100)
		err = ioutil.WriteFile(file, newContent, 0700)
		if err != nil {
			t.Fatal(err)
		}
		// Wait for the file to be healthy.
		err = build.Retry(1000, 100*time.Millisecond, func() error {
			// Get the fileinfo.
			file, err := r.RenterFileGet(rd.Files[i].SiaPath)
			if err != nil {
				t.Fatal(err)
			}
			// Check that the file is healthy.
			if file.File.MaxHealth > 1 {
				return fmt.Errorf("file is not healthy yet, threshold is %v but health is %v", 1, file.File.MaxHealth)
			}
			return nil
		})
		if err != nil {
			t.Fatal(err)
		}
		// Download the latest snapshot, untar it and check the contents.
		rf, err := r.RenterFileGet(rd.Files[i].SiaPath)
		if err != nil {
			t.Fatal(err)
		}
		_, err = r.RenterDownloadGet(rd.Files[i].SiaPath, tmpFile, 0, rf.File.Filesize, false)
		if err != nil {
			t.Fatal(err)
		}
		if err := untarDir(tmpFile, dir); err != nil {
			t.Fatal(err)
		}
		fis, err := ioutil.ReadDir(dir)
		if err != nil {
			t.Fatal(err)
		}
		if len(fis) != 1 {
			t.Fatalf("Snapshot contained %v file(s) but want %v", len(fis), 1)
		}
		if fis[0].Name() != filepath.Base(file) {
			t.Fatalf("Snapshot in file should have name %v but had name %v", filepath.Base(file), fis[0].Name())
		}
		readContent, err := ioutil.ReadFile(file)
		if err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(readContent, fileContent) {
			t.Log("Read content: ", readContent)
			t.Log("Original content: ", fileContent)
			t.Fatal("content of file in snapshot doesn't match original content")
		}
	}
}
