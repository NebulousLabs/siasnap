package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/NebulousLabs/Sia/node/api/client"
)

// appName is the name of the app which is used in various places throughout the
// codebase.
var appName = "siasnap"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   appName,
	Short: "A simple tool to create snapshots of directories and upload them to Sia",
	Long: `siasnap is a command-line tool to create snapshots of directories, 
	upload them to Sia and revert directories back to a snapshot`,
}

// siaClient is the http client talking to the Sia http API.
var siaClient = client.New("localhost:9980")

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	// TODO: Replace this value with the one in ~/.sia/apipassword.
	siaClient.Password = "f89baccf99e98fb8b28ff67aa1c72197"

	// Add commands.
	rootCmd.AddCommand(createCmd, listCmd, restoreCmd)

	// Execute.
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
