package cmd

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/Sia/build"
	"gitlab.com/NebulousLabs/fastrand"
)

// TestRestore tests the 'restore' function by manually uploading a snapshot
// first and then restoring from it.
func TestRestore(t *testing.T) {
	// Setup testgroup.
	tg, testDir, err := setupTestGroup(t.Name())
	if err != nil {
		t.Fatal(err)
	}
	// Create a path to a folder. It doesn't need to be created.
	dir := filepath.Join(testDir, "dir")
	if err := os.MkdirAll(dir, 0700); err != nil {
		t.Fatal(err)
	}
	file := filepath.Join(dir, "foo")
	fileContent := fastrand.Bytes(100)
	if err := ioutil.WriteFile(file, fileContent, 0700); err != nil {
		t.Fatal(err)
	}
	// Get the siapath of the dir.
	dirSiaPath, err := dirToSiaPath(dir)
	if err != nil {
		t.Fatal(err)
	}
	// Prepare a temp location for the snapshot.
	tmpFile := filepath.Join(testDir, "tmp.tar.gz")
	// Run this test multiple times to create multiples snapshots.
	renter := tg.Renters()[0]
	for i := 0; i < 3; i++ {
		// Create a snapshot of the dir.
		if err := os.RemoveAll(tmpFile); err != nil {
			t.Fatal(err)
		}
		if err := tarDir(dir, tmpFile); err != nil {
			t.Fatal(err)
		}
		// Upload the snapshot.
		snapSiaPath, err := dirSiaPath.Join(fmt.Sprint(i))
		if err != nil {
			t.Fatal(err)
		}
		if err := renter.RenterUploadPost(tmpFile, snapSiaPath, 1, 1); err != nil {
			t.Fatal(err)
		}
		// Wait for the file to be healthy.
		err = build.Retry(1000, 100*time.Millisecond, func() error {
			// Get the fileinfo.
			file, err := renter.RenterFileGet(snapSiaPath)
			if err != nil {
				t.Fatal(err)
			}
			// Check that the file is healthy.
			if file.File.MaxHealth > 1 {
				return fmt.Errorf("file is not healthy yet, threshold is %v but health is %v", 1, file.File.MaxHealth)
			}
			return nil
		})
		if err != nil {
			t.Fatal(err)
		}
		// Replace the content of the file with new content.
		newContent := fastrand.Bytes(100)
		err = ioutil.WriteFile(file, newContent, 0700)
		if err != nil {
			t.Fatal(err)
		}
		// Restore the snapshot using the 'restore' command.
		if err := restore(dir, fmt.Sprint(i)); err != nil {
			t.Fatal("Calling 'restore' returned an error", err)
		}
		// Compare the contents of the dir after restoring from the snapshot.
		fis, err := ioutil.ReadDir(dir)
		if err != nil {
			t.Fatal(err)
		}
		if len(fis) != 1 {
			t.Fatalf("Snapshot contained %v file(s) but want %v", len(fis), 1)
		}
		if fis[0].Name() != filepath.Base(file) {
			t.Fatalf("Snapshot in file should have name %v but had name %v", filepath.Base(file), fis[0].Name())
		}
		readContent, err := ioutil.ReadFile(file)
		if err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(readContent, fileContent) {
			t.Log("Read content: ", readContent)
			t.Log("Original content: ", fileContent)
			t.Fatal("content of file in snapshot doesn't match original content")
		}
	}
}
