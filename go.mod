module gitlab.com/NebulousLabs/siasnap

go 1.12

require (
	github.com/spf13/cobra v0.0.5
	gitlab.com/NebulousLabs/Sia v1.4.1
	gitlab.com/NebulousLabs/errors v0.0.0-20171229012116-7ead97ef90b8
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
)

replace (
	github.com/coreos/bbolt => ./vendor/github.com/coreos/bbolt
	github.com/xtaci/smux => ./vendor/github.com/xtaci/smux
)
