all:
	GO111MODULE=on go install

test-create:
	GO111MODULE=on go test -tags="testing debug" -v ./cmd -run=TestCreate

test-list:
	GO111MODULE=on go test -tags="testing debug" -v ./cmd -run=TestList

test-restore:
	GO111MODULE=on go test -tags="testing debug" -v ./cmd -run=TestRestore

test-helpers:
	GO111MODULE=on go test -tags="testing debug" -v ./cmd -run=TestHelpers

test-all:
	GO111MODULE=on go test -tags="testing debug" -v ./cmd -run=TestAll